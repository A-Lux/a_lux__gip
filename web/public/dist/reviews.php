<?php
    require_once("header.php");
?>
    <div class="reviews">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-xl-3">
                    <div class="d-flex justify-content-between">
                        <a class="current__page" href="#"><span>Главная</span></a><span>/</span><h4>Отзывы</h4>
                    </div>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-xl-12">
                    <h1>
                        Отзывы
                    </h1>
                </div>
                <div class="col-xl-12 comment--wrapper">
                    <div class="row justify-content-between">
                        <div class="col-xl-2 mobile__flex">
                            <h2>Оценка</h2>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                        </div>
                        <div class="col-xl-10">
                            <h2>Make <span>19.12.2019</span></h2>
                            <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.</p>
                            <div class="row justify-content-end mt-3">
                                <div class="col-xl-3 col-lg-4 col-sm-6 col-md-5 col-9">
                                    <div class="useful__wrapper">
                                        <h2>Отзыв полезен</h2>
                                        <span><button class="liked" type="button"></button></span>
                                        <span><button class="disliked" type="button"></button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 separator"></div>
                <div class="col-xl-12 comment--wrapper">
                    <div class="row justify-content-between">
                        <div class="col-xl-2 mobile__flex">
                            <h2>Оценка</h2>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                        </div>
                        <div class="col-xl-10">
                            <h2>Make <span>19.12.2019</span></h2>
                            <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.</p>
                            <div class="row justify-content-end mt-3">
                                <div class="col-xl-3 col-lg-4 col-sm-6 col-md-5 col-9">
                                    <div class="useful__wrapper">
                                        <h2>Отзыв полезен</h2>
                                        <span><button class="liked" type="button"></button></span>
                                        <span><button class="disliked" type="button"></button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 separator"></div>
                <div class="col-xl-12 comment--wrapper">
                    <div class="row justify-content-between">
                        <div class="col-xl-2 mobile__flex">
                            <h2>Оценка</h2>
                            <div class="star-rating__wrapper">
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="5">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="4">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="3">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="2">
                                </label>
                                <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                    <input class="star-rating__input" type="radio" name="rating" value="1">
                                </label>
                            </div>
                        </div>
                        <div class="col-xl-10">
                            <h2>Make <span>19.12.2019</span></h2>
                            <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.</p>
                            <div class="row justify-content-end mt-3">
                                <div class="col-xl-3 col-lg-4 col-sm-6 col-md-5 col-9">
                                    <div class="useful__wrapper">
                                        <h2>Отзыв полезен</h2>
                                        <span><button class="liked" type="button"></button></span>
                                        <span><button class="disliked" type="button"></button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 separator"></div>
                <div class="col-xl-3 mt-5">
                    <button class="leave__review" type="button">Оставить отзыв</button>
                </div>
            </div>
            <div class="row mt-5 mb-5">
                <div class="col-xl-6">
                    <div class="pagination__wrapper">
                        <button class="backward">Назад</button>
                        <a href="" class="item active">1</a>
                        <a href="" class="item">2</a>
                        <a href="" class="item">3</a>
                        <a href="" class="item">4</a>
                        <a href="" class="item">5</a>
                        <button class="forward">Вперед</button>
                    </div>
                </div>
                <div class="col-xl-6 d-flex justify-content-end">
                    <p class="total__services">Всего <span>18</span> Отзывов</p>
                </div>
            </div>
        </div>
    </div>
<?php
    require_once("footer.php");
?>