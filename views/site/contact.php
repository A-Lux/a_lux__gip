<div class="contact">
    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-xl-3 col-7">
                <?= $this->render('/partials/breadcrumbs', ['menu' => $menu]); ?>
            </div>
        </div>
        <div class="row"
             style="background: url(/public/dist/images/boxes.png) 95% 100% no-repeat; padding-bottom: 8rem;">
            <div class="col-xl-12">
                <h1>
                    <?= $menu->name; ?>
                </h1>
            </div>
        </div>
    </div>
    <div class="contacts__wrapper">
        <?= $contact->iframe; ?>
        <div class="container">
            <div class="row">
                <div class="col-xl-4">
                    <div class="contacts__block">
                        <h1><?= Yii::t('main', 'ТОО “G.I.P. Trans Group”') ?></h1>
                        <div class="d-flex align-items-center  mt-4">
                            <img src="/public/dist/images/address.png">
                            <p><?= $contact->address ?></p>
                        </div>
                        <div class="d-flex align-items-center  mt-4">
                            <img src="/public/dist/images/mail.png">
                            <a href="mailto:<?= $contact->email ?>"><?= $contact->email ?></a>
                        </div>
                        <div class="d-flex align-items-center  mt-4">
                            <img src="/public/dist/images/phone.png">
                            <div class="phone__wrapper">
                                <p><?= Yii::t('main', 'Городской') ?></p>
                                <a href="tel:<?= $contact->phone ?>"><?= $contact->phone ?></a>
                                <a href="https://wa.me/<?= $contact->mobile_phone ?>">
                                    <img src="/public/dist/images/whatsapp.png">
                                </a>
                            </div>
                        </div>
                        <div class="phone__wrapper mt-3">
                            <? if (!empty($contact->mobile_phone)): ?>
                                <p><?= Yii::t('main', 'Мобильный') ?></p>
                                <div class="d-flex">
                                    <a href="tel:<?= $contact->mobile_phone ?>"><?= $contact->mobile_phone ?></a>
                                </div>
                            <? endif; ?>
                        </div>
                        <div class="d-flex align-items-center mt-4">
                            <img src="/public/dist/images/time.png">
                            <p><?= $contact->mode ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
