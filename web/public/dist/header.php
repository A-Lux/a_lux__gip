<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GIP</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/main.css">
</head>
<body>
    <div class="header">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-xl-2 col-12 pl-0">
                    <div class="lang__wrapper">
                        <label class="lang__ico">
                            <img src="./images/kz_flag.png">
                            <input type="radio" name="lang" value="kz">
                        </label>
                        <label class="lang__ico">
                            <img src="./images/ru_flag.png">
                            <input type="radio" name="lang" value="ru">
                        </label>
                        <label class="lang__ico">
                            <img src="./images/uk_flag.png">
                            <input type="radio" name="lang" value="uk">
                        </label>
                        <label class="lang__ico">
                            <img src="./images/de_flag.png">
                            <input type="radio" name="lang" value="de">
                        </label>
                    </div>
                </div>
            </div>
            <div class="row pt-1 mb-5">
                <div class="col-xl-3 col-12">
                    <a href="#">
                        <img src="./images/logo.png">
                    </a>
                </div>
                <div class="col-xl-3 col-12">
                    <p>Транспортно-логистическая компания</p>
                </div>
                <div class="col-xl-3 col-12">
                    <div class="contacts">
                        <a class="telephone" href="tel: +7 (727) 317-16-98">+7 (727) 317-16-98</a>
                        <a class="email" href="mailto:giptransgroup@mail.ru">giptransgroup@mail.ru</a>
                    </div>
                </div>
                <div class="col-xl-3">
                    <button type="button" class="callback__btn">Заказать обратный звонок</button>
                </div>
            </div>
        </div>
        <div class="nav__wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 col-2">
                        <nav id="main-nav">
                            <ul class="main-nav">
                                <li><a href="./index.php"><span>Главная</span></a></li>
                                <li><a href="./about.php"><span>О компании</span></a></li>
                                <li><a href="./services.php"><span>Услуги</span></a></li>
                                <li><a href="./faq.php"><span>Вопрос-ответ</span></a></li>
                                <li><a href="./blog.php"><span>Блог</span></a></li>
                                <li><a href="./reviews.php"><span>Отзывы</span></a></li>
                                <li><a href="./contact.php"><span>Контакты</span></a></li>
                            </ul>
                        </nav>
                        <a class="hc-nav-trigger">
                            <span></span>
                        </a>
                    </div>
                    <div class="col-xl-3 col-10 d-flex justify-content-end">
                        <div class="social__wrapper">
                            <a href="https://instagram.com">
                                <img src="./images/instagram.png">
                            </a>
                            <a href="https://vk.com">
                                <img src="./images/vk.png">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    