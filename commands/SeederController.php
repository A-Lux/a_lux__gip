<?php

namespace app\commands;

use tebazil\yii2seeder\Seeder;
use Yii;
use yii\console\Controller;

class SeederController extends Controller
{
    public function actionFaker()
    {
        $seeder = new Seeder();
        $generator = $seeder->getGeneratorConfigurator();
        $faker = $generator->getFakerConfigurator();

        $seeder->table('menu')->columns([
            'id',
            'name'          => $faker->text(20),
            'status'        => 1,
            'url'           => $faker->slug,
            'sort'          => 1,
            'metaName'      => $faker->title,
            'metaDesc'      => $faker->title,
            'metaKey'       => $faker->title
        ])->rowQuantity(10);

//        $seeder->table('news')->columns([
//            'id',
//            'title'         => $faker->title,
//            'description'   => $faker->text,
//            'image'         => $faker->image(Yii::$app->basePath . '/web/uploads/images/news'),
//            'date'          => $faker->date(),
//            'metaName'      => $faker->title,
//            'metaDesc'      => $faker->title,
//            'metaKey'       => $faker->title
//        ])->rowQuantity(10);

        $seeder->refill();
    }
}