<?php
    require_once("header.php");
?>
    <div class="faq">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-xl-3">
                    <div class="d-flex justify-content-between">
                        <a class="current__page" href="#"><span>Главная</span></a><span>/</span><h4>Вопрос-ответ</h4>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-xl-12">
                    <h1>
                        Вопрос-ответ
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div id="accordion">
                        <div class="question__wrapper">
                            <div class="question-header">
                                <button class="question-toggle" type="button" data-toggle="collapse" data-target="#collapseOne" aria-controls="collapseOne" aria-expanded="false">
                                    <span></span>
                                </button>
                                <h5 class="question">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    Какое преимущество у меня будет при процедуре. Могут ли мне отказать в услуге?
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse"  data-parent="#accordion">
                                <div class="question-body">
                                    <p>При процедуре банкротства взыскание может быть обращено только на то имущество, которое не является необходимым для должника-гражданина. Тем более должника НЕ МОГУТ ЛИШИТЬ единственной КВАРТИРЫ или единственного жилого ДОМА. Согласно ч. 1 ст. 446 ГПК РФ взыскание по исполнительным документам не может быть обращено на следующее имущество, принадлежащее гражданину-должнику на праве собственности:</p>
                                    <ul>
                                        <li>
                                                Единственное жилье (дом или квартира) и земельный участок под застройкой;
                                        </li>
                                        <li>
                                                Предметы домашнего обихода и обстановки;
                                        </li>
                                        <li>
                                                Личные вещи (обувь, одежда и другие предметы индивидуального пользования);
                                        </li>
                                        <li>
                                                Имущество для профессиональной работы, за исключением предметов стоимостью более 100 МРОТ;
                                        </li>
                                        <li>
                                                Домашний скот, пчелы, птица;
                                        </li>
                                        <li>
                                                Домашний скот, пчелы, птица;
                                        </li>
                                        <li>
                                                Продукты питания и денежные суммы менее прожиточного минимума с учетом лиц, находящихся на иждивении;
                                        </li>
                                        <li>
                                                Топливо для отопления дома и приготовления пищи;
                                        </li>
                                        <li>
                                                Средства транспорта для инвалидов;
                                        </li>
                                        <li>
                                                Призы, награды, памятные знаки.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="question__wrapper">
                            <div class="question-header">
                                <button class="question-toggle" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-controls="collapseTwo" aria-expanded="false">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <h5 class="question">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Какое преимущество у меня будет при процедуре. Могут ли мне отказать в услуге?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="question-body">
                                    <p>При процедуре банкротства взыскание может быть обращено только на то имущество, которое не является необходимым для должника-гражданина. Тем более должника НЕ МОГУТ ЛИШИТЬ единственной КВАРТИРЫ или единственного жилого ДОМА. Согласно ч. 1 ст. 446 ГПК РФ взыскание по исполнительным документам не может быть обращено на следующее имущество, принадлежащее гражданину-должнику на праве собственности:</p>
                                    <ul>
                                        <li>
                                                Единственное жилье (дом или квартира) и земельный участок под застройкой;
                                        </li>
                                        <li>
                                                Предметы домашнего обихода и обстановки;
                                        </li>
                                        <li>
                                                Личные вещи (обувь, одежда и другие предметы индивидуального пользования);
                                        </li>
                                        <li>
                                                Имущество для профессиональной работы, за исключением предметов стоимостью более 100 МРОТ;
                                        </li>
                                        <li>
                                                Домашний скот, пчелы, птица;
                                        </li>
                                        <li>
                                                Домашний скот, пчелы, птица;
                                        </li>
                                        <li>
                                                Продукты питания и денежные суммы менее прожиточного минимума с учетом лиц, находящихся на иждивении;
                                        </li>
                                        <li>
                                                Топливо для отопления дома и приготовления пищи;
                                        </li>
                                        <li>
                                                Средства транспорта для инвалидов;
                                        </li>
                                        <li>
                                                Призы, награды, памятные знаки.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="question__wrapper">
                            <div class="question-header">
                                <button class="question-toggle" type="button" data-toggle="collapse" data-target="#collapseThree" aria-controls="collapseThree" aria-expanded="false">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <h5 class="question">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Какое преимущество у меня будет при процедуре. Могут ли мне отказать в услуге?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="question-body">
                                    <p>При процедуре банкротства взыскание может быть обращено только на то имущество, которое не является необходимым для должника-гражданина. Тем более должника НЕ МОГУТ ЛИШИТЬ единственной КВАРТИРЫ или единственного жилого ДОМА. Согласно ч. 1 ст. 446 ГПК РФ взыскание по исполнительным документам не может быть обращено на следующее имущество, принадлежащее гражданину-должнику на праве собственности:</p>
                                    <ul>
                                        <li>
                                                Единственное жилье (дом или квартира) и земельный участок под застройкой;
                                        </li>
                                        <li>
                                                Предметы домашнего обихода и обстановки;
                                        </li>
                                        <li>
                                                Личные вещи (обувь, одежда и другие предметы индивидуального пользования);
                                        </li>
                                        <li>
                                                Имущество для профессиональной работы, за исключением предметов стоимостью более 100 МРОТ;
                                        </li>
                                        <li>
                                                Домашний скот, пчелы, птица;
                                        </li>
                                        <li>
                                                Домашний скот, пчелы, птица;
                                        </li>
                                        <li>
                                                Продукты питания и денежные суммы менее прожиточного минимума с учетом лиц, находящихся на иждивении;
                                        </li>
                                        <li>
                                                Топливо для отопления дома и приготовления пищи;
                                        </li>
                                        <li>
                                                Средства транспорта для инвалидов;
                                        </li>
                                        <li>
                                                Призы, награды, памятные знаки.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="question__wrapper">
                            <div class="question-header">
                                <button class="question-toggle" type="button" data-toggle="collapse" data-target="#collapseFour" aria-controls="collapseFour" aria-expanded="false">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <h5 class="question">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Какое преимущество у меня будет при процедуре. Могут ли мне отказать в услуге?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseFour" class="collapse" data-parent="#accordion">
                                <div class="question-body">
                                    <p>При процедуре банкротства взыскание может быть обращено только на то имущество, которое не является необходимым для должника-гражданина. Тем более должника НЕ МОГУТ ЛИШИТЬ единственной КВАРТИРЫ или единственного жилого ДОМА. Согласно ч. 1 ст. 446 ГПК РФ взыскание по исполнительным документам не может быть обращено на следующее имущество, принадлежащее гражданину-должнику на праве собственности:</p>
                                    <ul>
                                        <li>
                                                Единственное жилье (дом или квартира) и земельный участок под застройкой;
                                        </li>
                                        <li>
                                                Предметы домашнего обихода и обстановки;
                                        </li>
                                        <li>
                                                Личные вещи (обувь, одежда и другие предметы индивидуального пользования);
                                        </li>
                                        <li>
                                                Имущество для профессиональной работы, за исключением предметов стоимостью более 100 МРОТ;
                                        </li>
                                        <li>
                                                Домашний скот, пчелы, птица;
                                        </li>
                                        <li>
                                                Домашний скот, пчелы, птица;
                                        </li>
                                        <li>
                                                Продукты питания и денежные суммы менее прожиточного минимума с учетом лиц, находящихся на иждивении;
                                        </li>
                                        <li>
                                                Топливо для отопления дома и приготовления пищи;
                                        </li>
                                        <li>
                                                Средства транспорта для инвалидов;
                                        </li>
                                        <li>
                                                Призы, награды, памятные знаки.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="question__wrapper">
                            <div class="question-header">
                                <button class="question-toggle" type="button" data-toggle="collapse" data-target="#collapseFive" aria-controls="collapseFive" aria-expanded="false">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <h5 class="question">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Какое преимущество у меня будет при процедуре. Могут ли мне отказать в услуге?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseFive" class="collapse" data-parent="#accordion">
                                <div class="question-body">
                                    <p>При процедуре банкротства взыскание может быть обращено только на то имущество, которое не является необходимым для должника-гражданина. Тем более должника НЕ МОГУТ ЛИШИТЬ единственной КВАРТИРЫ или единственного жилого ДОМА. Согласно ч. 1 ст. 446 ГПК РФ взыскание по исполнительным документам не может быть обращено на следующее имущество, принадлежащее гражданину-должнику на праве собственности:</p>
                                    <ul>
                                        <li>
                                                Единственное жилье (дом или квартира) и земельный участок под застройкой;
                                        </li>
                                        <li>
                                                Предметы домашнего обихода и обстановки;
                                        </li>
                                        <li>
                                                Личные вещи (обувь, одежда и другие предметы индивидуального пользования);
                                        </li>
                                        <li>
                                                Имущество для профессиональной работы, за исключением предметов стоимостью более 100 МРОТ;
                                        </li>
                                        <li>
                                                Домашний скот, пчелы, птица;
                                        </li>
                                        <li>
                                                Домашний скот, пчелы, птица;
                                        </li>
                                        <li>
                                                Продукты питания и денежные суммы менее прожиточного минимума с учетом лиц, находящихся на иждивении;
                                        </li>
                                        <li>
                                                Топливо для отопления дома и приготовления пищи;
                                        </li>
                                        <li>
                                                Средства транспорта для инвалидов;
                                        </li>
                                        <li>
                                                Призы, награды, памятные знаки.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="question__wrapper">
                            <div class="question-header">
                                <button class="question-toggle" type="button" data-toggle="collapse" data-target="#collapseSix" aria-controls="collapseSix" aria-expanded="false">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <h5 class="question">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    Какое преимущество у меня будет при процедуре. Могут ли мне отказать в услуге?
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseSix" class="collapse" data-parent="#accordion">
                                <div class="question-body">
                                    <p>При процедуре банкротства взыскание может быть обращено только на то имущество, которое не является необходимым для должника-гражданина. Тем более должника НЕ МОГУТ ЛИШИТЬ единственной КВАРТИРЫ или единственного жилого ДОМА. Согласно ч. 1 ст. 446 ГПК РФ взыскание по исполнительным документам не может быть обращено на следующее имущество, принадлежащее гражданину-должнику на праве собственности:</p>
                                    <ul>
                                        <li>
                                                Единственное жилье (дом или квартира) и земельный участок под застройкой;
                                        </li>
                                        <li>
                                                Предметы домашнего обихода и обстановки;
                                        </li>
                                        <li>
                                                Личные вещи (обувь, одежда и другие предметы индивидуального пользования);
                                        </li>
                                        <li>
                                                Имущество для профессиональной работы, за исключением предметов стоимостью более 100 МРОТ;
                                        </li>
                                        <li>
                                                Домашний скот, пчелы, птица;
                                        </li>
                                        <li>
                                                Домашний скот, пчелы, птица;
                                        </li>
                                        <li>
                                                Продукты питания и денежные суммы менее прожиточного минимума с учетом лиц, находящихся на иждивении;
                                        </li>
                                        <li>
                                                Топливо для отопления дома и приготовления пищи;
                                        </li>
                                        <li>
                                                Средства транспорта для инвалидов;
                                        </li>
                                        <li>
                                                Призы, награды, памятные знаки.
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5 mb-5">
                <div class="col-xl-6">
                    <div class="pagination__wrapper">
                        <button class="backward">Назад</button>
                        <a href="" class="item active">1</a>
                        <a href="" class="item">2</a>
                        <a href="" class="item">3</a>
                        <a href="" class="item">4</a>
                        <a href="" class="item">5</a>
                        <button class="forward">Вперед</button>
                    </div>
                </div>
                <div class="col-xl-6 d-flex justify-content-end">
                    <p class="total__services">Всего <span>18</span> Вопросов-ответов</p>
                </div>
            </div>
        </div>
    </div>
    
<?php
    require_once("footer.php");
?>