<?php

namespace app\controllers;

use app\models\About;
use app\models\Banner;
use app\models\Contacts;
use app\models\Logo;
use app\models\Menu;
use yii\web\Controller;
use Yii;

class FrontendController extends Controller
{
    public function init()
    {
        $menu               = Menu::find()->where(['status' => 1])->orderBy('sort ASC')->all();
        $banner             = Banner::find()->orderBy('sort ASC')->all();
        $contact            = Contacts::find()->orderBy('id ASC')->one();
        $about              = About::findOne(['status' => 1]);
        $logo_header        = Logo::find()->orderBy('id ASC')->one();
        $logo_footer        = Logo::find()->orderBy('id DESC')->one();


        \Yii::$app->view->params['menu']            = $menu;
        \Yii::$app->view->params['banner']          = $banner;
        \Yii::$app->view->params['contact']         = $contact;
        \Yii::$app->view->params['about']           = $about;
        \Yii::$app->view->params['logo_header']     = $logo_header;
        \Yii::$app->view->params['logo_footer']     = $logo_footer;

        parent::init(); // TODO: Change the autogenerated stub
    }

    protected function setMeta($title = null, $description = null, $keywords = null)
    {
        $this->view->title = $title;
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
    }

    private function setLang()
    {
        if(Yii::$app->session['language'] == '')
            Yii::$app->language = 'ru';
        elseif(Yii::$app->session['language'] == 'kk')
            Yii::$app->language = 'kk';
        elseif(Yii::$app->session['language'] == 'en')
            Yii::$app->language = 'en';
        elseif(Yii::$app->session['language'] == 'de')
            Yii::$app->language = 'de';
    }
}