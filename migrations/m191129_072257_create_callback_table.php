<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%callback}}`.
 */
class m191129_072257_create_callback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%callback}}', [
            'id' => $this->primaryKey(),
            'name'      => $this->string(255)->notNull(),
            'message'   => $this->text()->notNull(),
            'phone'     => $this->string(255)->notNull(),
            'email'     => $this->string(255)->notNull(),
            'date'      => $this->date()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%callback}}');
    }
}
