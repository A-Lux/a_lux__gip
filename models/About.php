<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "about".
 *
 * @property int $id
 * @property int $status
 * @property string $title
 * @property string|null $description
 * @property string|null $content
 * @property string|null $image
 *
 * @property AboutTranslation[] $aboutTranslations
 */
class About extends MultilingualActiveRecord
{
    public $path = 'uploads/images/about/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['description', 'content'], 'string'],
            [['status'], 'integer'],
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'title'         => 'Заголовок',
            'description'   => 'Краткое описание',
            'content'       => 'Описание',
            'image'         => 'Изображение',
            'status'        => 'Статус',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => AboutTranslation::className(),
                'tableName'     => AboutTranslation::tableName(),
                'attributes'    => ['title', 'description', 'content'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAboutTranslations()
    {
        return $this->hasMany(AboutTranslation::className(), ['id' => 'id']);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/about/' . $this->image : '';
    }
}
