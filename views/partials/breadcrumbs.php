<?php

/* @var $this yii\web\View */
/* @var $menu app\models\Menu */
/* @var $model  */

?>

    <? if(!empty($model)): ?>
        <div class="d-flex justify-content-between">
            <a class="current__page" href="/"><span><?= Yii::t('main', 'Главная'); ?></span></a>
            <span>/</span>
            <a class="current__page" href="<?= $menu->url; ?>"><h4><?= $menu->name; ?></h4><a>
            <span>/</span>
            <h4><?= $model->title; ?></h4>
        </div>
    <? else: ?>
        <div class="d-flex justify-content-between">
            <a class="current__page" href="/"><span><?= Yii::t('main', 'Главная'); ?></span></a>
            <span>/</span>
            <h4><?= $menu->name; ?></h4>
        </div>
    <? endif;?>
