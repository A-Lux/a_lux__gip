<?php

/* @var $this View */
/* @var $searchModel ServicesSearch */
/* @var $menu \app\models\Menu */

/* @var $dataProvider ActiveDataProvider */

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\models\search\ServicesSearch;

?>
    <div class="services">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-xl-3 col-md-4 col-sm-5 col-7">
                    <?= $this->render('/partials/breadcrumbs', compact('menu')) ?>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-xl-12 col-12">
                    <h1>
                        <?= $menu->name; ?>
                    </h1>
                </div>
            </div>
            <div class="row">
                <? foreach($dataProvider->models as $service): ?>
                    <div class="col-xl-4 col-md-4 col-sm-6 col-12">
                    <div class="service__block">
                        <img src="<?= $service->getImage(); ?>">
                        <h2><?= $service->title ?></h2>
<!--                        <a href="--><?//= Url::to(['/services/service', 'slug' => $service->slug]) ?><!--" class="more">Перейти</a>-->
                    </div>
                </div>
                <? endforeach;?>
            </div>
            <div class="row mt-5 mb-5">
                <div class="col-xl-6 col-md-8 col-sm-10 col-12">
                    <?= $this->render('/partials/pagination', ['dataProvider' => $dataProvider]) ?>
                </div>
                <div class="col-xl-6 col-md-4 col-sm-2 col-12 d-flex justify-content-end">
                    <p class="total__services">
                        <? $text = '<span>' . $count . '</span>'?>
                        <?= $count == 0 ? '' : Yii::t('main', 'Всего {text}  услуг',[
                            'text' => $text,
                        ]);?>
                    </p>
                </div>
            </div>
        </div>
    </div>
