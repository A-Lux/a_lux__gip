<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "callback".
 *
 * @property int $id
 * @property int $isRead
 * @property string $name
 * @property string $message
 * @property string $phone
 * @property string $email
 * @property string $date
 */
class Callback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'callback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['name', 'message', 'phone', 'email'], 'required'],
            [['message'], 'string'],
            [['isRead'], 'integer'],
            [['date'], 'safe'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'name'      => 'Имя',
            'message'   => 'Сообщение',
            'phone'     => 'Телефон',
            'email'     => 'Эл. почта',
            'date'      => 'Дата',
        ];
    }
}
