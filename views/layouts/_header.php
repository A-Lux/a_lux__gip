<?php

use yii\helpers\Url;

?>
<div class="header">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-xl-2 col-12 pl-0">
                    <div class="lang__wrapper">
                        <ul class="dropdown-menu lang__ico language-button selectpicker">
                            <li>
                                <a class="dropdown-item"
                                   href="<?= Url::to(array_merge(\Yii::$app->request->get(), [\Yii::$app->controller->route, 'language' => 'kk'])) ?>">
                                    <img src="/public/dist/images/kz_flag.png">
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item"
                                   href="<?= Url::to(array_merge(\Yii::$app->request->get(), [\Yii::$app->controller->route, 'language' => 'ru'])) ?>">
                                    <img src="/public/dist/images/ru_flag.png">
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item"
                                   href="<?= Url::to(array_merge(\Yii::$app->request->get(), [\Yii::$app->controller->route, 'language' => 'en'])) ?>">
                                    <img src="/public/dist/images/uk_flag.png">
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item"
                                   href="<?= Url::to(array_merge(\Yii::$app->request->get(), [\Yii::$app->controller->route, 'language' => 'de'])) ?>">
                                    <img src="/public/dist/images/de_flag.png">
                                </a>
                            </li>
                        </ul>
                        
                    </div>
                </div>
            </div>
            <div class="row pt-1 mb-5">
                <div class="col-xl-3 col-6">
                    <a href="/">
                        <img src="<?= \Yii::$app->view->params['logo_header']->getImage();?>">
                    </a>
                </div>
                <div class="col-xl-3 col-6">
                    <p><?= \Yii::$app->view->params['logo_header']->text; ?></p>
                </div>
                <div class="col-xl-3 col-6">
                    <div class="contacts">
                        <a class="telephone" href="tel:<?= \Yii::$app->view->params['contact']->phone; ?>">
                            <?= \Yii::$app->view->params['contact']->phone; ?>
                        </a>
                        <a class="email" href="mailto:<?= \Yii::$app->view->params['contact']->email; ?>">
                            <?= \Yii::$app->view->params['contact']->email; ?>
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-6">
                    <img src="/public/dist/images/call-answer.png" class="callback__btn">
                    
                </div>
            </div>
        </div>
        <div class="nav__wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 col-2">
                        <nav id="main-nav">
                            <ul class="main-nav">
                                <? foreach(Yii::$app->view->params['menu'] as $menu): ?>
                                    <li><a href="<?= $menu->url; ?>"><span><?= $menu->name; ?></span></a></li>
                                <? endforeach; ?>
                            </ul>
                        </nav>
                        <a class="toggle">
                            <span></span>
                        </a>
                    </div>
                    <div class="col-xl-3 col-10 d-flex justify-content-end">
                        <div class="social__wrapper">
                            <a href="<?= \Yii::$app->view->params['contact']->instagram; ?>" target="_blank">
                                <img src="/public/dist/images/instagram.png">
                            </a>
                            <a href="<?= \Yii::$app->view->params['contact']->vk; ?>" target="_blank">
                                <img src="/public/dist/images/vk.png">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    