<?php

namespace app\modules\admin\label;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelStatusAbout
{
    public static function statusList()
    {
        return [
            1 => 'На главном',
            0 => 'Нет',
        ];
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case 0:
                $class = 'label label-default';
                break;
            case 1:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }


}