<?php

namespace app\controllers;

use app\models\Feedback;
use app\models\Menu;
use app\models\QuestionAnswer;
use app\models\search\FeedbackSearch;
use app\models\search\QuestionAnswerSearch;
use app\models\search\ServicesSearch;
use app\models\Services;
use Codeception\Module\Yii1;

class ContentController extends FrontendController
{

    public function actionFaq()
    {
        $menu = Menu::findOne(['url' => '/content/faq']);
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $searchModel    = new QuestionAnswerSearch();
        $dataProvider   = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('faq', compact('menu', 'faq',
            'searchModel', 'dataProvider', 'count'));
    }

    public function actionReviews()
    {
        $menu = Menu::findOne(['url' => '/content/reviews']);
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $searchModel    = new FeedbackSearch();
        $dataProvider   = $searchModel->searchFront(\Yii::$app->request->queryParams);

        return $this->render('reviews', compact('searchModel', 'dataProvider', 'menu'));
    }

    public function actionServices()
    {
        $menu = Menu::findOne(['url' => '/content/services']);
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $services       = Services::find()->all();
        $searchModel    = new ServicesSearch();
        $dataProvider   = $searchModel->searchFront(\Yii::$app->request->queryParams);
        $count          = count($services);

        return $this->render('services', compact('menu', 'services', 'count',
            'searchModel', 'dataProvider'));
    }

    public function actionService($slug)
    {
        $menu   = Menu::findOne(['url' => '/blog/articles']);

        $service = Services::findOne(['slug' => $slug]);
        $this->setMeta($service->metaName, $service->metaDesc, $service->metaKey);

        return $this->render('service', compact('service', 'menu'));
    }

    public function actionFeedback()
    {
        $model  = new Feedback();

        if($model->load(\Yii::$app->request->post()) && $model->validate()){
            $model->date    = \Yii::$app->formatter->asDate(time(), 'Y-MM-d');

            \Yii::$app->session->setFlash('success', \Yii::t('main', 'Благодарим за Ваш отзыв!'));

            if($model->save()){
                return $this->redirect('/content/reviews');
            }
        }

        return $this->render('review', compact('model'));
    }


    public function actionLike() 
    {
        $id = $_GET['id'];
        if($model = Feedback::findOne(['id' => $id])){
            $model->like    += 1;
            $model->save();

            $like   = $model->like;
            $response = ['status' => 1, 'like' => $like];

            return json_encode($response);
        }else{
            $response = ['status' => 0];
            return json_encode($response);
        }
    }

    public function actionDislike()
    {
         $id = $_GET['id'];
        if($model = Feedback::findOne(['id' => $id])){
            $model->dislike    += 1;
            $model->save();

            $dislike   = $model->dislike;
            $response = ['status' => 1, 'dislike' => $dislike];

            return json_encode($response);
        }else{
            $response = ['status' => 0];
            return json_encode($response);
        }
    }
}