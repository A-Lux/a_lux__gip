<?php

use yii\helpers\Html;
use app\widgets\MultilingualDetailView;

/* @var $this yii\web\View */
/* @var $model app\models\QuestionAnswer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Вопрос-Ответ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="question-answer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'format' => 'raw',
                'attribute' => 'question',
                'value' => function($data){
                    return $data->question;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'answer',
                'value' => function($data){
                    return $data->answer;
                }
            ],
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\label\LabelStatus::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\label\LabelStatus::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
