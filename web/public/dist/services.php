<?php
    require_once("header.php");
?>
    <div class="services">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-xl-3">
                    <div class="d-flex justify-content-between">
                        <a class="current__page" href="#"><span>Главная</span></a><span>/</span><h4>Услуги</h4>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-xl-12">
                    <h1>
                        Услуги
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4">
                    <div class="service__block">
                        <img src="./images/service__img.png">
                        <h2>Перевозка техники</h2>
                        <a href="" class="more">Перейти</a>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="service__block">
                        <img src="./images/service__img.png">
                        <h2>Перевозка техники</h2>
                        <a href="" class="more">Перейти</a>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="service__block">
                        <img src="./images/service__img.png">
                        <h2>Перевозка техники</h2>
                        <a href="" class="more">Перейти</a>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="service__block">
                        <img src="./images/service__img.png">
                        <h2>Перевозка техники</h2>
                        <a href="" class="more">Перейти</a>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="service__block">
                        <img src="./images/service__img.png">
                        <h2>Перевозка техники</h2>
                        <a href="" class="more">Перейти</a>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="service__block">
                        <img src="./images/service__img.png">
                        <h2>Перевозка техники</h2>
                        <a href="" class="more">Перейти</a>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="service__block">
                        <img src="./images/service__img.png">
                        <h2>Перевозка техники</h2>
                        <a href="" class="more">Перейти</a>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="service__block">
                        <img src="./images/service__img.png">
                        <h2>Перевозка техники</h2>
                        <a href="" class="more">Перейти</a>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="service__block">
                        <img src="./images/service__img.png">
                        <h2>Перевозка техники</h2>
                        <a href="" class="more">Перейти</a>
                    </div>
                </div>
            </div>
            <div class="row mt-5 mb-5">
                <div class="col-xl-6">
                    <div class="pagination__wrapper">
                        <button class="backward">Назад</button>
                        <a href="" class="item active">1</a>
                        <a href="" class="item">2</a>
                        <a href="" class="item">3</a>
                        <a href="" class="item">4</a>
                        <a href="" class="item">5</a>
                        <button class="forward">Вперед</button>
                    </div>
                </div>
                <div class="col-xl-6 d-flex justify-content-end">
                    <p class="total__services">Всего <span>18</span> услуг</p>
                </div>
            </div>
        </div>
    </div>
<?php
    require_once("footer.php");
?>