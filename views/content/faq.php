<?php

/* @var $this View */
/* @var $searchModel QuestionAnswerSearch */
/* @var $menu \app\models\Menu */

/* @var $dataProvider ActiveDataProvider */

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\models\search\QuestionAnswerSearch;

$count  = count($dataProvider->models);

?>
    <div class="faq">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-xl-3 col-md-4 col-sm-6 col-8">
                    <?= $this->render('/partials/breadcrumbs', compact('menu')) ?>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-xl-12 col-12">
                    <h1>
                        <?= $menu->name; ?>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-12">
                    <div id="accordion">
                        <? foreach($dataProvider->models as $item): ?>
                            <div class="question__wrapper">
                                <div class="question-header">
                                    <button class="question-toggle" type="button" data-toggle="collapse" data-target="#collapse-<?= $item->id; ?>" aria-controls="collapse-<?= $item->id; ?>" aria-expanded="false">
                                        <span></span>
                                    </button>
                                    <h5 class="question">
                                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-<?= $item->id; ?>" aria-expanded="false" aria-controls="collapse-<?= $item->id; ?>">
                                            <?= $item->question; ?>
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapse-<?= $item->id; ?>" class="collapse"  data-parent="#accordion">
                                    <div class="question-body">
                                        <?= $item->answer; ?>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="row mt-5 mb-5">
                <div class="col-xl-6 col-md-8 col-sm-10 col-12">
                    <?= $this->render('/partials/pagination', [
                            'dataProvider' => $dataProvider,
                    ]) ?>
                </div>
                <div class="col-xl-6 col-md-4 col-sm-2 col-12 d-flex justify-content-end">
                    <p class="total__services">
                        <? $text = '<span>' . $count . '</span>'?>
                        <?= $count == 0 ? '' : Yii::t('main', 'Всего {text}  Вопросов-ответов',[
                            'text' => $text,
                        ]);?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    
