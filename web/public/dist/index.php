<?php
    require_once("header.php");
?>
    <div class="home">
        <div class="main__banner owl-carousel owl-theme">
            <div class="item" style="width: 100%;background-image: url('./images/carousel__background.png')">
                <div class="container d-flex flex-column h-100" style="justify-content: space-evenly;">
                    <div class="row mt-5">
                        <div class="col-xl-6">
                            <h1>Транспортно-логистическая компания</h1>
                        </div>
                        <div class="col-xl-6"></div>
                        <div class="col-xl-5 mt-4">
                            <p>
                                Имеем большой опыт решения самых сложных логистических задач в области перевозки
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-3">
                            <a href="#" class="more">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item" style="background-image: url('./images/carousel__background.png')">
                <div class="container d-flex flex-column h-100" style="justify-content: space-evenly;">
                    <div class="row mt-5">
                        <div class="col-xl-6">
                            <h1>Транспортно-логистическая компания</h1>
                        </div>
                        <div class="col-xl-6"></div>
                        <div class="col-xl-5 mt-4">
                            <p>
                                Имеем большой опыт решения самых сложных логистических задач в области перевозки
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-3">
                            <a href="#" class="more">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item" style="background-image: url('./images/carousel__background.png')">
                <div class="container d-flex flex-column h-100" style="justify-content: space-evenly;">
                    <div class="row mt-5">
                        <div class="col-xl-6">
                            <h1>Транспортно-логистическая компания</h1>
                        </div>
                        <div class="col-xl-6"></div>
                        <div class="col-xl-5 mt-4">
                            <p>
                                Имеем большой опыт решения самых сложных логистических задач в области перевозки
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-3">
                            <a href="#" class="more">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item" style="background-image: url('./images/carousel__background.png')">
                <div class="container d-flex flex-column h-100" style="justify-content: space-evenly;">
                    <div class="row mt-5">
                        <div class="col-xl-6">
                            <h1>Транспортно-логистическая компания</h1>
                        </div>
                        <div class="col-xl-6"></div>
                        <div class="col-xl-5 mt-4">
                            <p>
                                Имеем большой опыт решения самых сложных логистических задач в области перевозки
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-3">
                            <a href="#" class="more">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="assets__wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 d-flex justify-content-between align-items-center">
                        <img src="./images/travel.png">
                        <p>Перевозки по всем направлениям</p>
                    </div>
                    <div class="col-xl-3 d-flex justify-content-between align-items-center">
                        <img src="./images/reward.png">
                        <p>Многолетний опыт сотрудников</p>
                    </div>
                    <div class="col-xl-3 d-flex justify-content-between align-items-center">
                        <img src="./images/truck.png">
                        <p>Собственный парк автомашин</p>
                    </div>
                    <div class="col-xl-3 d-flex justify-content-between align-items-center">
                        <img src="./images/heart.png">
                        <p>Внимание к каждому клиенту</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="about__wrapper">
            <div class="container mt-5 mb-5">
                <div class="row">
                    <div class="col-xl-6">
                        <h1>О компании</h1>
                        <p>Компания G.I.P. Trans Group. Мы организовываем и осуществляем перевозки различных грузов в и из Казахстана, обеспечиваем транзит в страны Центральной Азии всеми видами транспорта. Наша компания активно работает на границах и переходах Казахстана с Китаем, Россией и странами Центральной Азии.</p>
                        <a href="#" class="more">Подробнее</a>
                    </div>
                    <div class="col-xl-6">
                        <img src="./images/about.png">
                    </div>
                </div>
            </div>
        </div>
        <div class="services__wrapper">
            <div class="container">
                <div class="row pt-5">
                    <div class="col-xl-12">
                        <h1>Услуги</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4">
                        <div class="service__block">
                            <img src="./images/service__img.png">
                            <h2>Перевозка техники</h2>
                            <a href="#" class="more">Перейти</a>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="service__block">
                            <img src="./images/service__img.png">
                            <h2>Перевозка техники</h2>
                            <a href="#" class="more">Перейти</a>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="service__block">
                            <img src="./images/service__img.png">
                            <h2>Перевозка техники</h2>
                            <a href="#" class="more">Перейти</a>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="service__block">
                            <img src="./images/service__img.png">
                            <h2>Перевозка техники</h2>
                            <a href="#" class="more">Перейти</a>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="service__block">
                            <img src="./images/service__img.png">
                            <h2>Перевозка техники</h2>
                            <a href="#" class="more">Перейти</a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center mt-5 pb-5">
                    <div class="col-xl-4">
                        <a href="#" class="continue">Перейти к списку услуг</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="reviews__wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 pt-5">
                        <h1>Отзывы</h1>
                        <p>Вы также можете оставить свой отзыв о работе с нами!</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="reviews__banner owl-carousel owl-theme">
            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-3">
                            <div class="d-flex flex-column">
                                <div class="avatar" style="background-image: url('./images/girl.png');"></div>
                                <h2 class="initials">Виктория Рогова</h2>
                                <p class="date">17 мая</p>
                            </div>
                        </div>
                        <div class="col-xl-9">
                            <div class="message__block">
                                <div class="star-rating__wrapper">
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="5">
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="3">
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="2">
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="1">
                                    </label>
                                </div>
                                <p class="message">Воспользовались услугами по перевозке Дмитрия, был большой квартирный переезд - много мебели, личных вещей, бытовой техники. Дмитрий и 2 грузчиков все очень оперативно погрузили, доставили, все приехало в целостности и сохранности. Мы остались очень довольны, спасибо за то что сработали без опозданий, с пониманием отнеслись к заминкам с нашей стороны (вещи не упаковали хорошо, консьержка очень сильно мешала работать). Сэкономили нам кучу нервов, оцениваю предоставленные услуги на 10/10.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-3">
                            <div class="d-flex flex-column">
                                <div class="avatar" style="background-image: url('./images/girl.png');"></div>
                                <h2 class="initials">Виктория Рогова</h2>
                                <p class="date">17 мая</p>
                            </div>
                        </div>
                        <div class="col-xl-9">
                            <div class="message__block">
                                <div class="star-rating__wrapper">
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="5">
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="3">
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="2">
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="1">
                                    </label>
                                </div>
                                <p class="message">Воспользовались услугами по перевозке Дмитрия, был большой квартирный переезд - много мебели, личных вещей, бытовой техники. Дмитрий и 2 грузчиков все очень оперативно погрузили, доставили, все приехало в целостности и сохранности. Мы остались очень довольны, спасибо за то что сработали без опозданий, с пониманием отнеслись к заминкам с нашей стороны (вещи не упаковали хорошо, консьержка очень сильно мешала работать). Сэкономили нам кучу нервов, оцениваю предоставленные услуги на 10/10.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-3">
                            <div class="d-flex flex-column">
                                <div class="avatar" style="background-image: url('./images/girl.png');"></div>
                                <h2 class="initials">Виктория Рогова</h2>
                                <p class="date">17 мая</p>
                            </div>
                        </div>
                        <div class="col-xl-9">
                            <div class="message__block">
                                <div class="star-rating__wrapper">
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="5">
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="3">
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="2">
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="1">
                                    </label>
                                </div>
                                <p class="message">Воспользовались услугами по перевозке Дмитрия, был большой квартирный переезд - много мебели, личных вещей, бытовой техники. Дмитрий и 2 грузчиков все очень оперативно погрузили, доставили, все приехало в целостности и сохранности. Мы остались очень довольны, спасибо за то что сработали без опозданий, с пониманием отнеслись к заминкам с нашей стороны (вещи не упаковали хорошо, консьержка очень сильно мешала работать). Сэкономили нам кучу нервов, оцениваю предоставленные услуги на 10/10.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-3">
                            <div class="d-flex flex-column">
                                <div class="avatar" style="background-image: url('./images/girl.png');"></div>
                                <h2 class="initials">Виктория Рогова</h2>
                                <p class="date">17 мая</p>
                            </div>
                        </div>
                        <div class="col-xl-9">
                            <div class="message__block">
                                <div class="star-rating__wrapper">
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="5">
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="4" checked>
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="3">
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="2">
                                    </label>
                                    <label class="star-rating__ico star-rating__hover fa fa-star fa-lg">
                                        <input class="star-rating__input" type="radio" name="rating" value="1">
                                    </label>
                                </div>
                                <p class="message">Воспользовались услугами по перевозке Дмитрия, был большой квартирный переезд - много мебели, личных вещей, бытовой техники. Дмитрий и 2 грузчиков все очень оперативно погрузили, доставили, все приехало в целостности и сохранности. Мы остались очень довольны, спасибо за то что сработали без опозданий, с пониманием отнеслись к заминкам с нашей стороны (вещи не упаковали хорошо, консьержка очень сильно мешала работать). Сэкономили нам кучу нервов, оцениваю предоставленные услуги на 10/10.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all__reviews-wrapper">
            <div class="container">
                <div class="row justify-content-center mt-5 pb-5">
                    <div class="col-xl-4">
                        <a href="#" class="continue">Читать все отзывы</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="contacts__wrapper">
            <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A5fcd6e2f7422fb8093e76e59f1118eb35615456bf065d58587107235ad6cf2d4&amp;source=constructor" width="100%" frameborder="0"></iframe>
            <div class="container">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="contacts__block">
                            <h1>Контакты</h1>
                            <p>Казахстана, г. Нур-Султан ул. Прохорова 29</p>
                            <a href="tel: +7 (727) 317-16-98">+7 (727) 317-16-98</a>
                            <a href="#" class="continue">Заказать обратный звонок</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    require_once("footer.php");
?>