<?php

namespace app\controllers;

use app\models\About;
use app\models\Advantages;
use app\models\Banner;
use app\models\Callback;
use app\models\Contacts;
use app\models\Feedback;
use app\models\i18n;
use app\models\Menu;
use app\models\search\AboutSearch;
use app\models\Services;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $menu = Menu::findOne(['url' => '/']);
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $banner     = Banner::find()->orderBy('sort ASC')->all();
        $advantages = Advantages::find()->orderBy('sort ASC')->all();
        $about      = About::findOne(['status' => 1]);
        $services   = Services::find()->orderBy('id DESC')->limit(6)->all();
        $feedback   = Feedback::find()->where(['status' => 1])->all();
        $contact    = Contacts::find()->orderBy('id ASC')->one();

        return $this->render('index', compact(
            'banner', 'advantages', 'about', 'services', 'feedback', 'contact'
        ));
    }

    public function actionAbout()
    {
        $menu = Menu::findOne(['url' => '/site/about']);
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $searchModel    = new AboutSearch();
        $dataProvider   = $searchModel->searchView(Yii::$app->request->queryParams);
        $contact        = Contacts::find()->orderBy('id ASC')->one();

        return $this->render('about', compact('menu', 'searchModel', 'dataProvider', 'contact'));
    }

    public function actionContact()
    {
        $menu = Menu::findOne(['url' => '/site/contact']);
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $contact = Contacts::find()->orderBy('id ASC')->one();

        return $this->render('contact', compact('contact', 'menu'));
    }

    public function beforeAction($action)
    {
        if($action->id == 'callback'){
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionFeedback()
    {
        $model          = new Feedback();
        $model->date    = Yii::$app->formatter->asDate(time(), 'Y-MM-dd');

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return 1;
        }else{
            return 0;
        }
    }

    public function actionTest()
    {
        var_dump(Yii::$app->language);
    }

}
