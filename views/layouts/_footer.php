<?php

use yii\helpers\Url;
?>
<!-- Mobile Navigation -->
<nav id="mob-nav">
    <ul class="main-nav">
        <? foreach(Yii::$app->view->params['menu'] as $menu): ?>
            <li><a href="<?= $menu->url; ?>"><span><?= $menu->name; ?></span></a></li>
        <? endforeach; ?>
        <li>
            <span><?= Yii::t('main', 'Языки') ?></span>
            <ul>
                <li>
                    <a class="dropdown-item d-flex align-items-center"
                       href="<?= Url::to(array_merge(\Yii::$app->request->get(), [\Yii::$app->controller->route, 'language' => 'kk'])) ?>">
                        <img src="/public/dist/images/kz_flag.png">
                        <span><?= Yii::t('main', 'Казахский') ?></span>
                    </a>
                </li>
                <li>
                    <a class="dropdown-item d-flex align-items-center"
                       href="<?= Url::to(array_merge(\Yii::$app->request->get(), [\Yii::$app->controller->route, 'language' => 'ru'])) ?>">
                        <img src="/public/dist/images/ru_flag.png">
                        <span><?= Yii::t('main', 'Русский') ?></span>
                    </a>
                </li>
                <li>
                    <a class="dropdown-item d-flex align-items-center"
                       href="<?= Url::to(array_merge(\Yii::$app->request->get(), [\Yii::$app->controller->route, 'language' => 'en'])) ?>">
                        <img src="/public/dist/images/uk_flag.png">
                        <span><?= Yii::t('main', 'Английский') ?></span>
                    </a>
                </li>
                <li>
                    <a class="dropdown-item d-flex align-items-center"
                       href="<?= Url::to(array_merge(\Yii::$app->request->get(), [\Yii::$app->controller->route, 'language' => 'de'])) ?>">
                        <img src="/public/dist/images/de_flag.png">
                        <span><?= Yii::t('main', 'Немецкий') ?></span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
<div class="footer">
    <div class="container">
        <div class="row pt-1 mb-5">
            <div class="col-xl-3 col-12">
                <a href="/">
                    <img src="<?= \Yii::$app->view->params['logo_footer']->getImage(); ?>">
                </a>
            </div>
            <div class="col-xl-3 col-12">
                <p><?= \Yii::$app->view->params['logo_footer']->text; ?></p>
            </div>
            <div class="col-xl-3 col-12">
                <div class="contacts">
                    <a class="telephone" href="tel:<?= \Yii::$app->view->params['contact']->phone; ?>">
                        <?= \Yii::$app->view->params['contact']->phone; ?>
                    </a>
                    <a class="email" href="mailto:<?= \Yii::$app->view->params['contact']->email; ?>">
                        <?= \Yii::$app->view->params['contact']->email; ?>
                    </a>
                </div>
            </div>
            <div class="col-xl-3">
            </div>
        </div>
    </div>
    <div class="nav__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xl-9">
                    <ul class="main-nav">
                        <? foreach (Yii::$app->view->params['menu'] as $menu): ?>
                            <li><a href="<?= $menu->url; ?>"><span><?= $menu->name; ?></span></a></li>
                        <? endforeach; ?>
                    </ul>
                </div>
                <div class="col-xl-3 d-flex justify-content-end">
                    <div class="social__wrapper">
                        <a href="<?= \Yii::$app->view->params['contact']->instagram; ?>" target="_blank">
                            <img src="/public/dist/images/instagram.png">
                        </a>
                        <a href="<?= \Yii::$app->view->params['contact']->vk; ?>" target="_blank">
                            <img src="/public/dist/images/vk.png">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="rights">
        <div class="container pt-3 pb-3">
            <div class="row">
                <div class="col-xl-3">
                    <p><?= Yii::t('main', '© 2010-2019 «G.I.P. Trans Group»') ?></p>
                </div>
                <div class="col-xl-3">
                
                </div>
                <div class="col-xl-3">
                    <? $text = '<a target="_blank" href="https://a-lux.kz">A-LUX</a>';?>
                    <p><?= Yii::t('main', 'Сайт разработан в {text}',[
                            'text' => $text,
                        ])?>
                    </p>
                </div>
                <div class="col-xl-3">
                    <p><?= Yii::t('main', 'Политика конфиденциальности') ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>