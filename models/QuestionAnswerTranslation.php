<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "question_answer_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $question
 * @property string|null $answer
 *
 * @property QuestionAnswer $id0
 */
class QuestionAnswerTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'question_answer_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['answer'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['question'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => QuestionAnswer::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'question' => 'Question',
            'answer' => 'Answer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(QuestionAnswer::className(), ['id' => 'id']);
    }
}
