<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "services_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $title
 * @property string|null $description
 * @property string|null $content
 * @property string|null $metaName
 * @property string|null $metaDesc
 * @property string|null $metaKey
 *
 * @property Services $id0
 */
class ServicesTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['description', 'content', 'metaDesc', 'metaKey'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['title', 'metaName'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Services::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'title' => 'Title',
            'description' => 'Description',
            'content' => 'Content',
            'metaName' => 'Meta Name',
            'metaDesc' => 'Meta Desc',
            'metaKey' => 'Meta Key',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Services::className(), ['id' => 'id']);
    }
}
