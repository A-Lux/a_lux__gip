
    <div class="blog">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-xl-2 col-sm-3 col-md-2 col-6">
                    <?= $this->render('/partials/breadcrumbs', [
                            'menu'  => $menu,
                            'model' => $article,
                    ]) ?>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-xl-12 col-12">
                    <h1>
                        <?= $article->title; ?>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-12">
                    <div class="article__content">
                        <?= $article->description; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-12">
                    <?= $article->content; ?>
                </div>
            </div>
        </div>
    </div>
