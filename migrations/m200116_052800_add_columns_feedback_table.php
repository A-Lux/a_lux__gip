<?php

use yii\db\Migration;

/**
 * Class m200116_052800_add_columns_feedback_table
 */
class m200116_052800_add_columns_feedback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('feedback', 'like', $this->integer()->defaultValue(0)->notNull());
        $this->addColumn('feedback', 'dislike', $this->integer()->defaultValue(0)->notNull());
        $this->addColumn('feedback', 'status', $this->integer()->defaultValue(0)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('feedback', 'like');
        $this->dropColumn('feedback', 'dislike');
        $this->dropColumn('feedback', 'status');
    }
}
