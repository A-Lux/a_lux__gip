<?php

/* @var $this View */
/* @var $service Services */
/* @var $menu \app\models\Menu */

use yii\helpers\Url;
use yii\web\View;
use app\models\Services;

?>

    <div class="service">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-xl-4 col-10">
                    <?= $this->render('/partials/pagination', [
                            'menu'  => $menu,
                            'model' => $service,
                    ]) ?>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-xl-12 col-12">
                    <h1><?= $service->title; ?></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-12">
                    <div class="service__block">
                        <img src="<?= $service->getImage(); ?>">
                        <?= $service->content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>