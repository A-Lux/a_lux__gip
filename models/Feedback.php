<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property string $name
 * @property string|null $content
 * @property string $date
 * @property int|null $star
 * @property int|null $status
 * @property int|null $like
 * @property int|null $dislike
 *
 * @property FeedbackTranslation[] $feedbackTranslations
 */
class Feedback extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['content'], 'string'],
            [['date'], 'safe'],
            [['star', 'status', 'like', 'dislike'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'        => 'ID',
            'name'      => 'ФИО',
            'content'   => 'Содержание',
            'date'      => 'Дата',
            'star'      => 'Оценка',
            'status'    => 'Статус отзыва',
            'like'      => 'Лайк',
            'dislike'   => 'Дизлайк',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => FeedbackTranslation::className(),
                'tableName'     => FeedbackTranslation::tableName(),
                'attributes'    => ['name', 'content'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbackTranslations()
    {
        return $this->hasMany(FeedbackTranslation::className(), ['id' => 'id']);
    }
}
