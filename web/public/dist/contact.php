<?php
    require_once("header.php");
?>
    <div class="contact">
        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-xl-3">
                    <div class="d-flex justify-content-between">
                        <a class="current__page" href="#"><span>Главная</span></a><span>/</span><h4>Контакты</h4>
                    </div>
                </div>
            </div>
            <div class="row" style="background: url(../images/boxes.png) 95% 100% no-repeat; padding-bottom: 8rem;">
                <div class="col-xl-12">
                    <h1>
                        Контакты
                    </h1>
                </div>
            </div>
        </div>
        <div class="contacts__wrapper">
                <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A5fcd6e2f7422fb8093e76e59f1118eb35615456bf065d58587107235ad6cf2d4&amp;source=constructor" width="100%" frameborder="0"></iframe>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4">
                            <div class="contacts__block">
                                <h1>ТОО “G.I.P. Trans Group”</h1>
                                <div class="d-flex align-items-center  mt-4">
                                    <img src="./images/address.png">
                                    <p>Казахстана, г. Нур-Султан ул. Прохорова 29</p>
                                </div>
                                <div class="d-flex align-items-center  mt-4">
                                    <img src="./images/mail.png">
                                    <a href="mailto: GIPTRANSGROUP@mail.ru">GIPTRANSGROUP@mail.ru</a>
                                </div>
                                <div class="d-flex align-items-center  mt-4">
                                    <img src="./images/phone.png">
                                    <div class="phone__wrapper">
                                        <p>Городской</p>
                                        <a href="tel: +7 (727) 317-16-98">+7 (727) 317-16-98</a>
                                    </div>
                                </div>
                                <div class="phone__wrapper mt-3">
                                    <p>Мобильный</p>
                                    <div class="d-flex">
                                        <a href="tel: +7 (727) 317-16-98">+7 (727) 317-16-98</a>
                                        <div class="d-flex ml-5">
                                            <a href="https://wa.me/77273171698">
                                                <img src="./images/whatsapp.png">
                                            </a>
                                            <a href="viber://chat?number=+77273171698">
                                                <img src="./images/viber.png">
                                            </a>
                                        </div>
                                    </div>
                                </div>  
                                <div class="d-flex align-items-center mt-4">
                                    <img src="./images/time.png">
                                    <p>Работаем с <span>9<sup>00</sup></span> до <span>18<sup>00</sup></span>
                                        Вс - <span>Выходной</span></p>
                                </div>
                                <a href="javascript:void();" class="continue mt-3">Заказать обратный звонок</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
<?php
    require_once("footer.php");
?>
