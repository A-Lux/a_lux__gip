<?php

/* @var $this View */
/* @var $searchModel FeedbackSearch */
/* @var $menu \app\models\Menu */

/* @var $dataProvider ActiveDataProvider */

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\models\search\FeedbackSearch;

$count  = count($dataProvider->models);

?>
<div class="reviews">
    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-xl-3 col-md-3 col-sm-4 col-7">
                <?= $this->render('/partials/breadcrumbs', compact('menu')) ?>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-xl-12 col-12">
                <h1>
                    <?= $menu->name; ?>
                </h1>
            </div>
            <? foreach($dataProvider->models as $model): ?>
                <div class="col-xl-12 col-12 comment--wrapper">
                    <div class="row justify-content-between">
                        <div class="col-xl-2 col-md-2 col-sm-3 col-12 mobile__flex">
                            <h2><?= Yii::t('main', 'Оценка') ?></h2>
                            <div class="star-rating__wrapper" data-star="<?= $model->star;?>">
                               
                            </div>
                        </div>
                        <div class="col-xl-10 col-md-10 col-sm-9 col-12">
                            <h2><?= $model->name; ?> <span><?= $model->date;?></span></h2>
                            <p><?= $model->content; ?></p>
                            <div class="row justify-content-end mt-3 uw">
                                <div class="col-xl-3 col-lg-4 col-sm-7 col-md-5 col-9">
                                    <div class="useful__wrapper">
                                        <h2><?= Yii::t('main', 'Отзыв полезен') ?></h2>
                                        <span data-like="<?= $model->like; ?>"><button class="liked" type="button"  data-id="<?= $model->id;?>"></button></span>
                                        <span data-dislike="<?= $model->dislike; ?>"><button class="disliked" type="button"  data-id="<?= $model->id;?>"></button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-12 separator"></div>
            <? endforeach; ?>
            <div class="col-xl-3 col-sm-5 col-12 mt-5">
                <a href="/content/feedback" class="leave__review" type="button"><?= Yii::t('main', 'Оставить отзыв') ?></a>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-xl-6 col-md-8 col-sm-10 col-12">
                <?= $this->render('/partials/pagination', [
                        'dataProvider' => $dataProvider,
                ]) ?>
            </div>
            <div class="col-xl-6 col-md-4 col-sm-2 col-12 d-flex justify-content-end">
                <p class="total__services">
                    <? $text = '<span>' . $count . '</span>'?>
                    <?= $count == 0 ? '' : Yii::t('main', 'Всего {text}  Отзывов',[
                        'text' => $text,
                    ]);?>
                </p>
            </div>
        </div>
    </div>
</div>
