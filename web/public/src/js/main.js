import 'popper.js';
import 'bootstrap';
window.$ = window.jQuery = require('jquery');
const owl_carousel = require('owl.carousel');
const offcanvas = require('hc-offcanvas-nav');
const Swal = require('sweetalert2');
const axios = require('axios');
import 'jquery-mask-plugin';
$(document).ready(function() {
    let $Nav = $("#mob-nav");
    let $Toggle = $('.toggle');
    let defaultData = {
        maxWidth: 1025,
        customToggle: $Toggle,
        navTitle: 'Навигация',
        levelTitles: true,
        pushContent: 'body > *',
        insertClose: /*11*/ false,
        labelBack: 'Назад',
       /* labelClose: 'Закрыть',*/
        closeLevels: false
    };
    let Navigation = $Nav.hcOffcanvasNav(defaultData);
    const update = (settings) => {
        if (Navigation.isOpen()) {
            Navigation.on('close.once', function() {
            Navigation.update(settings);
            Navigation.open();
        });

        Navigation.close();
        }
        else {
        Navigation.update(settings);
            }
        };
   
    $('.footer .nav__wrapper .col-xl-3.d-flex.justify-content-end').addClass('justify-content-center');
    function callUs() {
        Swal.fire({
            title: 'Введите номер телефона',
            input: 'text',
            showLoaderOnConfirm: true,
            inputAttributes: {
                name: 'phone'
            },
            onOpen: function () {
                $('.swal2-input').mask('+0 (000) 000 00 00', {placeholder: "+_ (___) ___ __ __"});
            },
            preConfirm: (phone) => {
                const data = new FormData();
                data.append('phone', phone)
                return axios.post('http://gip.loc:8080/site/callback', data)
                    .then(response => console.log(response))
                    .catch(error => {
                        Swal.showValidationMessage(
                            `Request failed: ${error}`
                        )
                    });
            },
            allowOutsideClick: () => !Swal.isLoading()
        })
        .then(data => {
            Swal.fire({
                title: data.statusText
            });
        });
    }
    $('.callback__btn').on('click', function() {
        callUs();
    });
    if (window.matchMedia("(max-width: 576px)").matches) {
        // $("").addClass("order-10");
        $(".header .row.pt-1.mb-5").addClass("flex-row-reverse");
    }
    
});
function starCount() {
    let wrappers = document.querySelectorAll('.star-rating__wrapper');
    let label; let input;
    wrappers.forEach((wrapper) => {
        if (wrapper.childElementCount < 5) {
            for (let i = 5; i > wrapper.getAttribute('data-star'); i--) {
                label = document.createElement('label');
                input = document.createElement('input');
                input.classList.add('star-rating__input');
                input.setAttribute('type', 'radio');
                input.setAttribute('name', 'rating');
                label.classList.add('star-rating__ico', 'fa', 'fa-star', 'fa-lg');
                label.appendChild(input);
                wrapper.appendChild(label);
            }
        }
        for (let i = 0; i < wrapper.getAttribute('data-star'); i++) {
            label = document.createElement('label');
            input = document.createElement('input');
            input.classList.add('star-rating__input');
            input.setAttribute('type', 'radio');
            input.setAttribute('name', 'rating');
            label.classList.add('star-rating__ico', 'star-rating__checked', 'fa', 'fa-star', 'fa-lg');
            label.appendChild(input);
            wrapper.appendChild(label);
        }
    });
}
if ($("div").is(".home")) {
    $(document).ready(function() {
        $('.main__banner').owlCarousel({
            loop:true,
            items: 1,
            nav: true,
            dots: true,
            responsiveClass:true,
            center: true,
            autoplay: true,
            autoplayTimeout: 5000,
            responsive:{
                0:{
                    nav: true,
                    center: true,
                },
                600:{
                    nav: true,
                    center: false,
                },
                1000:{
                    nav: true,
                    center: false,
                }
            }
        });
        $('.reviews__banner').owlCarousel({
            loop:true,
            items: 2,
            nav: true,
            dots: true,
            center: true,
            margin: 20,
            responsive:{
                0:{
                    nav: false,
                    margin: 0,
                    items: 1,
                },
                600:{
                    nav: true,
                    margin: 10,
                    items: 1,
                },
                1000:{
                    nav: true,
                    margin: 20,
                    items: 2,
                }
            }
        });
        document.getElementsByClassName('owl-prev')[0].children[0].innerText = "";
        document.getElementsByClassName('owl-next')[0].children[0].innerText = "";
        document.getElementsByClassName('owl-prev')[1].children[0].innerText = "";
        document.getElementsByClassName('owl-next')[1].children[0].innerText = "";
        function owlDotsWrap(el) {
            let owldots = el;
            let container = document.createElement("div");
            container.classList.add("container");
            let row = document.createElement("div");
            row.classList.add("row");
            let col = document.createElement("div");
            col.classList.add("col-12");
            owldots.parentNode.insertBefore(col, owldots);
            col.appendChild(owldots); 
            col.parentNode.insertBefore(row, col);
            row.appendChild(col);
            row.parentNode.insertBefore(container, row);
            container.appendChild(row);
        }
        function owlNavWrap(el) {
            let owldots = el;
            let container = document.createElement("div");
            container.classList.add("container-fluid");
            let row = document.createElement("div");
            row.classList.add("row");
            let col = document.createElement("div");
            col.classList.add("col-12");
            col.classList.add("position-static");
            owldots.parentNode.insertBefore(col, owldots);
            col.appendChild(owldots); 
            col.parentNode.insertBefore(row, col);
            row.appendChild(col);
            row.parentNode.insertBefore(container, row);
            container.appendChild(row); 
        }
        owlDotsWrap(document.getElementsByClassName("owl-dots")[0]);
        owlNavWrap(document.getElementsByClassName("owl-nav")[0]);
        owlDotsWrap(document.getElementsByClassName("owl-dots")[1]);
        owlNavWrap(document.getElementsByClassName("owl-nav")[1]);
        if (window.matchMedia("(max-width: 576px)").matches) {
            $('.assets__wrapper .col-xl-3.d-flex.justify-content-between.align-items-center').addClass('flex-column mt-3');
            $('.reviews__banner .owl-item .item .d-flex.flex-column').addClass('align-items-center');
        }
    });
    starCount();
}
else if ($("div").is(".faq")) {
    $(document).ready(function() {
        $(".question-toggle").on("click", function(e) {
            e.preventDefault();
            $(this).toggleClass("active");
        });
        $(".question").on("click", function(e) {
            e.preventDefault();
            $(".question").not(this).prev().removeClass("active");
            $(this).prev().toggleClass("active");
        });
    });
}
else if ($("div").is(".about")) {
    document.getElementsByClassName('about__wrapper')[0].children[0].children[0].children[1].classList.add = 'order-10';
}
else if ($("div").is(".reviews")) {
    starCount();
    document.querySelectorAll('.useful__wrapper').forEach((usefull) => {
        usefull.children[1].innerHTML += usefull.children[1].getAttribute('data-like');
        usefull.children[2].innerHTML += usefull.children[2].getAttribute('data-dislike');
    });
    function removeAllText(element) {

        // loop through all the nodes of the element
        var nodes = element.childNodes;
    
        for(var i = 0; i < nodes.length; i++) {
    
            var node = nodes[i];
    
            // if it's a text node, remove it
            if(node.nodeType == Node.TEXT_NODE) {
    
                node.parentNode.removeChild(node);
    
    
                i--; // have to update our incrementor since we just removed a node from childNodes
    
            } else
    
            // if it's an element, repeat this process
            if(node.nodeType == Node.ELEMENT_NODE) {
    
                removeAllText(node);
    
            }
        }
    }
    $('.liked').on('click', function() {
        let id = $(this).attr("data-id");
        let me = $(this);
        $.ajax({
            type: 'GET',
            url: '/content/like',
            dataType: 'JSON',
            data: {id: id},
            success: function(response) {
                console.log(response);
                let likes =  response.like;
                console.log(response.like);
                removeAllText($(me)[0].parentNode);
                $(me)[0].parentNode.innerHTML += likes;
            }
        });
    })
    $('.disliked').on('click', function() {
        let id = $(this).attr("data-id");
        let me = $(this);
        $.ajax({
            type: 'GET',
            url: '/content/dislike',
            dataType: 'JSON',
            data: {id: id},
            success: function(response) {
                console.log(response);
                let dislike =  response.dislike;
                console.log(response.dislike);
                removeAllText($(me)[0].parentNode);
                $(me)[0].parentNode.innerHTML += dislike;
            }
        });
    })
}

$('.star-rating__input').on('click', function() {
    let value = $(this).val();
    $('.star-rating-active-form')[0].setAttribute('value', value);
});
