<?php

namespace app\controllers;

use app\models\Menu;
use app\models\News;
use app\models\search\NewsSearch;

class BlogController extends FrontendController
{
    public function actionArticles()
    {
        $menu = Menu::findOne(['url' => '/blog/articles']);
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $searchModel    = new NewsSearch();
        $dataProvider   = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('articles', compact('menu', 'searchModel', 'dataProvider'));
    }

    public function actionArticle($slug)
    {
        $menu   = Menu::findOne(['url' => '/blog/articles']);

        $article = News::findOne(['slug' => $slug]);
        $this->setMeta($article->metaName, $article->metaDesc, $article->metaKey);

        return $this->render('blog', compact('article', 'menu'));
    }
}