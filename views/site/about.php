<?php

/* @var $this View */
/* @var $searchModel AboutSearch */
/* @var $menu \app\models\Menu */
/* @var $contact \app\models\Contacts */

/* @var $dataProvider ActiveDataProvider */

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\models\search\AboutSearch;

$n = 0;

?>
<div class="about">
    <div class="container">
        <div class="row mt-5 mb-5">
            <div class="col-xl-3 col-8">
                <?= $this->render('/partials/breadcrumbs', compact('menu')) ?>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-xl-12 col-12">
                <h1>
                    <?= $menu->name; ?>
                </h1>
            </div>
        </div>
    </div>

    <? foreach ($dataProvider->models as $model): ?>
    <? $n++;?>
        <div class="about__wrapper">
            <div class="container">
                <div class="row">
                    <div class="<?= $n == 1 ? 'col-xl-7 col-12 mt-5' : 'col-xl-6 col-12'; ?>">
                        <h2><?= $model->title;?></h2>
                        <p>
                            <?= $model->content; ?>
                        </p>
                    </div>
                    <div class="<?= $n == 1 ? 'col-xl-5 col-12' : 'col-xl-6 col-12'; ?>">
                        <img src="<?= $model->getImage(); ?>">
                    </div>
                </div>
            </div>
        </div>
    <? endforeach;?>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-12">
                <div class="arrow__cloud">
                    <p>
                        <?= Yii::t('main', 'По всем вопросам вы можете связаться с нашими специалистами по адресу') ?>
                    </p>
                    <a href="mailto:<?= $contact->email;?>"><?= $contact->email;?></a>
                    <div class="background"></div>
                </div>
            </div>
        </div>
    </div>
</div>
