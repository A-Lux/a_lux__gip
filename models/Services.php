<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "services".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string|null $description
 * @property string|null $content
 * @property string|null $image
 * @property string|null $metaName
 * @property string|null $metaDesc
 * @property string|null $metaKey
 *
 * @property ServicesTranslation[] $servicesTranslations
 */
class Services extends MultilingualActiveRecord
{
    public $path = "uploads/images/services/";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'content', 'metaDesc', 'metaKey', 'slug'], 'string'],
            [['title', 'image', 'metaName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'title'         => 'Заголовок',
            'description'   => 'Краткое описание',
            'content'       => 'Контент',
            'image'         => 'Изображение',
            'metaName'      => 'Мета название',
            'metaDesc'      => 'Мета описание',
            'metaKey'       => 'Мета ключи',
            'slug'          => 'slug',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => ServicesTranslation::className(),
                'tableName'     => ServicesTranslation::tableName(),
                'attributes'    => ['title', 'description', 'content', 'metaName', 'metaDesc', 'metaKey'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicesTranslations()
    {
        return $this->hasMany(ServicesTranslation::className(), ['id' => 'id']);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/services/' . $this->image : '';
    }
}
