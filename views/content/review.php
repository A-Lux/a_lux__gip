<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */

?>


<div class="container mt-5 mb-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><?= Yii::t('main-form', 'Оставьте отзыв') ?></div>

                <div class="card-body">
                    <? $form = ActiveForm::begin([
                        'id'      => 'feedback-form',
                        'action'  => ['/content/feedback'],
                    ]) ?>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">
                                <?= Yii::t('main-form', 'Имя') ?>
                            </label>
                            <div class="col-md-6">
                                <?= $form->field($model, 'name')->textInput([
                                    'id'            => 'name',
                                    'maxlength'     => true,
                                    'class'         => 'form-control',
                                    'placeholder'   => Yii::t('main-form', 'Имя'),
                                    'autofocus'     => true,
                                    'required'      => 'required'
                                ])->label(false) ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="review__area" class="col-md-4 col-form-label text-md-right">
                                <?= Yii::t('main-form', 'Отзыв') ?>
                            </label>
                            <div class="col-md-6">
                                <?= $form->field($model, 'content')->textarea([
                                    'id'            => 'review__area',
                                    'maxlength'     => true,
                                    'class'         => 'form-control',
                                    'required'      => 'required'
                                ])->label(false) ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <?= $form->field($model, 'star')->textInput([
                                'hidden' => true,
                                'class' => 'star-rating-active-form',
                            ])->label(false) ?>
                            <label for="star-rating" class="col-md-4 col-form-label text-md-right">
                                <?= Yii::t('main-form', 'Оценка') ?>
                            </label>
                            <div class="col-md-6 d-flex align-items-center">
                                <div class="star-rating" id="star-rating">
                                    <div class="star-rating__wrap">
                                        <input id="star-rating-5" class="star-rating__input swal-input3" type="radio"
                                               name="rating" value="5">
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-5">
                                        </label>
                                        <input id="star-rating-4" class="star-rating__input swal-input3" type="radio"
                                               name="rating" value="4">
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-4">
                                        </label>
                                        <input id="star-rating-3" class="star-rating__input swal-input3" type="radio"
                                               name="rating" value="3">
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-3">
                                        </label>
                                        <input id="star-rating-2" class="star-rating__input swal-input3" type="radio"
                                               name="rating" value="2">
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-2">
                                        </label>
                                        <input id="star-rating-1" class="star-rating__input swal-input3" type="radio"
                                               name="rating" value="1">
                                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-1">
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                
                                <?= Html::submitButton(Yii::t('main-form', 'Отправить'), ['class' => 'btn btn-primary']) ?>
                            </div>
                        </div>
                    <? ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
</div>