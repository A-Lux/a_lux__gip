<div class="home">
    <div class="main__banner owl-carousel owl-theme">
        <? foreach ($banner as $item): ?>
            <div class="item" style="width: 100%;background-image: url('<?= $item->getImage(); ?>')">
                <div class="container d-flex flex-column h-100" style="justify-content: space-evenly;">
                    <div class="row mt-5">
                        <div class="col-xl-6 col-12">
                            <h1><?= !empty($item->title) ? $item->title : ''; ?></h1>
                        </div>
                        <div class="col-xl-6 col-12"></div>
                        <div class="col-xl-5 col-12 mt-4">
                            <p>
                                <?= !empty($item->description) ? $item->description : ''; ?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <? if ($item->statusButton == 1): ?>
                            <div class="col-xl-3 col-12">
                                <a href="<?= $item->linkButton ?>" class="more">
                                    <?= Yii::t('main', 'Подробнее'); ?>
                                </a>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        <? endforeach; ?>

    </div>
    <div class="assets__wrapper">
        <div class="container">
            <div class="row">
                <? foreach ($advantages as $item): ?>
                    <div class="col-xl-3 col-6 d-flex justify-content-between align-items-center">
                        <img src="<?= $item->getImage(); ?>">
                        <p><?= $item->description ?></p>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
    <div class="about__wrapper">
        <div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-xl-6 col-12">
                    <h1><?= $about->title; ?></h1>
                    <?= $about->content; ?>
                    <a href="<?= \yii\helpers\Url::to(['/site/about']); ?>" class="more">
                        <?= Yii::t('main', 'Подробнее'); ?>
                    </a>
                </div>
                <div class="col-xl-6 col-12">
                    <img src="<?= $about->getImage(); ?>">
                </div>
            </div>
        </div>
    </div>
    <div class="services__wrapper">
        <? if (!empty($services)): ?>
            <div class="container">
                <div class="row pt-5">
                    <div class="col-xl-12 col-12">
                        <h1><?= Yii::t('main', 'Услуги'); ?></h1>
                    </div>
                </div>
                <div class="row">
                    <? foreach ($services as $item): ?>
                        <div class="col-xl-4 col-12">
                            <div class="service__block">
                                <img src="<?= $item->getImage(); ?>">
                                <h2><?= $item->title; ?></h2>
<!--                                <a href="--><?//= \yii\helpers\Url::to(['/services/service', 'slug' => $item->slug]); ?><!--"-->
<!--                                   class="more">-->
<!--                                    Перейти-->
<!--                                </a>-->
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
                <div class="row justify-content-center mt-5 pb-5">
                    <div class="col-xl-4 col-12">
                        <a href="/content/services" class="continue">
                            <?= Yii::t('main', 'Перейти к списку услуг'); ?>
                        </a>
                    </div>
                </div>
            </div>
        <? endif; ?>
    </div>
    <? if (!empty($feedback)): ?>
        <div class="reviews__wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-12 pt-5">
                        <h1><?= Yii::t('main', 'Отзывы'); ?></h1>
                        <p><?= Yii::t('main', 'Вы также можете оставить свой отзыв о работе с нами!'); ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="reviews__banner owl-carousel owl-theme">
            <? foreach ($feedback as $item): ?>
                <div class="item">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-3 col-12">
                                <div class="d-flex flex-column">

                                    <h2 class="initials"><?= $item->name; ?></h2>
                                    <p class="date"><?= $item->date ?></p>
                                </div>
                            </div>
                            <div class="col-xl-9 col-12">
                                <div class="message__block">
                                    <div class="star-rating__wrapper"  data-star="<?= $item->star;?>">
                                    </div>
                                    <p class="message">
                                        <?= $item->content; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
        <div class="all__reviews-wrapper">
            <div class="container">
                <div class="row justify-content-center mt-5 pb-5">
                    <div class="col-xl-4 col-12">
                        <a href="/content/reviews" class="continue">
                            <?= Yii::t('main', 'Читать все отзывы'); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <? endif; ?>
    <div class="contacts__wrapper">
        <?= $contact->iframe; ?>
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-12">
                    <div class="contacts__block">
                        <h1><?= Yii::t('main', 'Контакты'); ?></h1>
                        <p><?= $contact->address ?></p>
                        <a href="tel: <?= $contact->phone ?>"><?= $contact->phone ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

