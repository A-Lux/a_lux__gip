<?php

namespace app\controllers;

use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class LangController extends Controller
{
    public function actionIndex($language)
    {
        if($language == 'ru'){
            \Yii::$app->session->set('language', '');
        }elseif ($language == 'kk'){
            \Yii::$app->session->set('language', 'kk');
        }elseif ($language == 'en'){
            \Yii::$app->session->set('language', 'en');
        }elseif ($language == 'de'){
            \Yii::$app->session->set('language', 'de');
        }else{
            throw new ForbiddenHttpException;
        }

        $this->redirect($_SERVER['HTTP_REFERER']);
    }
}