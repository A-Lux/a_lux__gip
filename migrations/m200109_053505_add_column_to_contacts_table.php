<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%contacts}}`.
 */
class m200109_053505_add_column_to_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contacts', 'mobile_phone', $this->string(255)->null());
        $this->addColumn('contacts', 'mode', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contacts', 'mobile_phone');
        $this->dropColumn('contacts', 'mode');
    }
}
