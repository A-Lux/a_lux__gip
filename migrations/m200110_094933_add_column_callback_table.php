<?php

use yii\db\Migration;

/**
 * Class m200110_094933_add_column_callback_table
 */
class m200110_094933_add_column_callback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('callback', 'isRead', $this->integer()->defaultValue(0)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('callback', 'isRead');
    }
}
