<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%contacts_translation}}`.
 */
class m200109_054907_add_column_to_contacts_translation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contacts_translation', 'mode', $this->text()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contacts_translation', 'mode');
    }
}
