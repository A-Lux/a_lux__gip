<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "question_answer".
 *
 * @property int $id
 * @property string $question
 * @property string|null $answer
 * @property int $status
 *
 * @property QuestionAnswerTranslation[] $questionAnswerTranslations
 */
class QuestionAnswer extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'question_answer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question'], 'required'],
            [['answer'], 'string'],
            [['status'], 'integer'],
            [['question'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'        => 'ID',
            'question'  => 'Вопрос',
            'answer'    => 'Ответ',
            'status'    => 'Статус',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => QuestionAnswerTranslation::className(),
                'tableName'     => QuestionAnswerTranslation::tableName(),
                'attributes'    => ['question', 'answer'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestionAnswerTranslations()
    {
        return $this->hasMany(QuestionAnswerTranslation::className(), ['id' => 'id']);
    }
}
