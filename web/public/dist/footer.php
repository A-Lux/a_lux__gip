    <div class="footer">
        <div class="container">
            <div class="row pt-1 mb-5">
                <div class="col-xl-3">
                    <a href="#">
                        <img src="./images/footer__logo.png">
                    </a>
                </div>
                <div class="col-xl-3">
                    <p>Транспортно-логистическая компания</p>
                </div>
                <div class="col-xl-3">
                    <div class="contacts">
                        <a class="telephone" href="tel: +7 (727) 317-16-98">+7 (727) 317-16-98</a>
                        <a class="email" href="mailto:giptransgroup@mail.ru">giptransgroup@mail.ru</a>
                    </div>
                </div>
                <div class="col-xl-3">
                    <button type="button" class="callback__btn">Заказать обратный звонок</button>
                </div>
            </div>
        </div>
        <div class="nav__wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9">
                        <ul class="main-nav">
                            <li><a href="#"><span>Главная</span></a></li>
                            <li><a href="#"><span>О компании</span></a></li>
                            <li><a href="#"><span>Услуги</span></a></li>
                            <li><a href="#"><span>Вопрос-ответ</span></a></li>
                            <li><a href="#"><span>Блог</span></a></li>
                            <li><a href="#"><span>Отзывы</span></a></li>
                            <li><a href="#"><span>Контакты</span></a></li>
                        </ul>
                    </div>
                    <div class="col-xl-3 d-flex justify-content-end">
                        <div class="social__wrapper">
                            <a href="https://instagram.com">
                                <img src="./images/instagram.png">
                            </a>
                            <a href="https://vk.com">
                                <img src="./images/vk.png">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="./js/main.js"></script>
</body>
</html>