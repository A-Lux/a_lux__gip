<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $address
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $instagram
 * @property string|null $vk
 * @property string|null $iframe
 * @property string|null $mobile_phone
 * @property string|null $mode
 *
 * @property ContactsTranslation[] $contactsTranslations
 */
class Contacts extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'required'],
            [['iframe', 'mode'], 'string'],
            [['address', 'phone', 'email', 'instagram', 'vk', 'mobile_phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'address'       => 'Адрес',
            'phone'         => 'Телефон',
            'email'         => 'Эл. почта',
            'instagram'     => 'Инстаграм',
            'vk'            => 'Вк',
            'iframe'        => 'Карта',
            'mobile_phone'  => 'Мобильный телефон',
            'mode'          => 'Режим работы',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => ContactsTranslation::className(),
                'tableName'     => ContactsTranslation::tableName(),
                'attributes'    => ['address', 'mode'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsTranslations()
    {
        return $this->hasMany(ContactsTranslation::className(), ['id' => 'id']);
    }
}
